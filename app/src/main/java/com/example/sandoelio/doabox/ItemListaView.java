package com.example.sandoelio.doabox;

import java.io.Serializable;

public class ItemListaView implements Serializable {

    private String instituicao, objetivo, ligacao, estado, cidade, necessidade, login ,senha ;
    private int iconeRid;

    public ItemListaView(String instituicao, String objetivo, String ligacao, String estado,
                        String cidade, String necessidade, String login, String senha,int iconeRid) {
        this.instituicao = instituicao;
        this.objetivo = objetivo;
        this.ligacao = ligacao;
        this.estado = estado;
        this.cidade = cidade;
        this.necessidade = necessidade;
        this.login = login;
        this.senha = senha;
        this.iconeRid = iconeRid;
    }

    public String getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(String instituicao) {
        this.instituicao = instituicao;
    }

    public String getObjetivo() {
        return "Objetivo : " + objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

     public String getLigacao() {
         return "Ligacao : " + ligacao;
     }

     public void setLigacao(String ligacao) {
         this.ligacao = ligacao;
     }

     public String getEstado() {
         return "Estado : " + estado;
     }

     public void setEstado(String estado) {
         this.estado = estado;
     }

     public String getCidade() {
         return "Cidade : " + cidade;
     }

     public void setCidade(String cidade) {
         this.cidade = cidade;
     }

     public String getNecessidade() {
         return "Necessidade : " + necessidade;
     }

     public void setNecessidade(String necessidade) {
         this.necessidade = necessidade;
     }

     public String getLogin() {
         return login;
     }

     public void setLogin(String login) {
         this.login = login;
     }

     public String getSenha() {
         return senha;
     }

     public void setSenha(String senha) {
         this.senha = senha;
     }

    public int getIconeRid() {
        return iconeRid;
    }

}

