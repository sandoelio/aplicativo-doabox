package com.example.sandoelio.doabox.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*----Responsavel pela  criação e versionamento do banco ------*/
public class CriaBanco extends SQLiteOpenHelper{

    public static final String NOME_BANCO = "banco.db";
    public static final String TABELA = "entidade";
    public static final String ID = "_id";
    public static final String INSTITUICAO = "instituicao";
    public static final String OBJETIVO = "objetivo";
    public static final String TELEFONE = "ligacao";
    public static final String CIDADE = "cidade";
    public static final String ESTADO = "estado";
    public static final String NECESSIDADE = "necessidade";
    public static final String LOGIN = "login";
    public static final String SENHA = "senha";
    public static final int VERSAO = 1;

    /*-----construtor que passará as informações do local e versão do banco-----*/
    public CriaBanco(Context context){
        super(context, NOME_BANCO,null,VERSAO);
    }

    /*----------------Responsavel pela manipulação do banco----------------*/
    public void onCreate(SQLiteDatabase db) {
       //onCreate cria o banco de dados pela primeira vez
        String sql = "CREATE TABLE "+ TABELA +" ("
                + ID + " integer primary key autoincrement,"
                + INSTITUICAO + " text,"
                + OBJETIVO + " text,"
                + TELEFONE + " text,"
                + CIDADE + " text,"
                + ESTADO + " text,"
                + NECESSIDADE + " text,"
                + LOGIN + " text,"
                + SENHA + " text"
                + " )";

        db.execSQL(sql);
    }

    /*--------------Responsavel pela atualização do banco----------------*/
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABELA);
        onCreate(db);
    }
}