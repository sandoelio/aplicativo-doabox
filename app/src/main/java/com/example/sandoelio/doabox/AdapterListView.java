package com.example.sandoelio.doabox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class AdapterListView extends BaseAdapter{
    private LayoutInflater mInflater;
    private ArrayList<ItemListaView> itens;

    public AdapterListView(Context context, ArrayList<ItemListaView> itens) {

        //Itens que preencheram o listview
        this.itens = itens;

        //responsavel por pegar o Layout do item
        mInflater = LayoutInflater.from(context);
    }

    /*-----------metodo Retorna a quantidade de itens-----------*/
    public int getCount() {
        return itens.size();
    }

    /*---------metodo Retorna o item de acordo com a posicao----------*/
    public ItemListaView getItem(int position) {
        return itens.get(position);
    }

    /*---------metodo Retorna o id de acordo com a posicao------------*/
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {

        //Pega o item de acordo com a posição
        ItemListaView item = itens.get(position);

        //infla o layout para podermos preencher a view
        view = mInflater.inflate(R.layout.modelo_lista, null);

        // pegamos cada id relacionado ao item e define as informações
        ((TextView) view.findViewById(R.id.instituicao)).setText(item.getInstituicao());
        ((TextView) view.findViewById(R.id.objetivo)).setText(item.getObjetivo());
        ((TextView) view.findViewById(R.id.ligacao)).setText(item.getLigacao());
        ((TextView) view.findViewById(R.id.estado)).setText(item.getEstado());
        ((TextView) view.findViewById(R.id.cidade)).setText(item.getCidade());
        ((TextView) view.findViewById(R.id.necessidade)).setText(item.getNecessidade());
        ((TextView) view.findViewById(R.id.login)).setText(item.getLogin());
        ((TextView) view.findViewById(R.id.senha)).setText(item.getSenha());
        ((ImageView)view.findViewById(R.id.imagemView)).setImageResource(item.getIconeRid());

        return view;
    }
}

