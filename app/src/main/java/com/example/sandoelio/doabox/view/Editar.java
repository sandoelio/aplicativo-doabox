package com.example.sandoelio.doabox.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sandoelio.doabox.R;
import com.example.sandoelio.doabox.banco.BancoController;
import com.example.sandoelio.doabox.banco.CriaBanco;

import java.util.HashMap;
import java.util.Map;

public class Editar extends Activity {

    private EditText instituicao,objetivo,ligacao,estado,cidade,necessidade,login,senha;
    private Button   editar,deletar,sair;
    private Cursor   cursor;
    private BancoController crud;
    private String codigo;
    private Map<String,String> dadosInstituicao = new HashMap<String, String>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        //Metodo para fonte (letras)
        String fontPath = "Ban.ttf";
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView2.setTypeface(tf);

        //recuperado o código passado pela Intent
        codigo = this.getIntent().getStringExtra("codigo");

        //instanciar a classe que faz o controle do banco
        crud = new BancoController(getBaseContext());

        //capturando os valores dos campos pelo ID
        instituicao = findViewById(R.id.altInstituicao);
        objetivo    = findViewById(R.id.altObjetivo);
        ligacao    = findViewById(R.id.altLigacao);
        estado      = findViewById(R.id.altEstado);
        cidade      = findViewById(R.id.altCidade);
        necessidade = findViewById(R.id.altNecessidade);
        login       = findViewById(R.id.altLogin);
        senha       = findViewById(R.id.altSenha);
        sair        = findViewById(R.id.btnsair2);
        editar      = findViewById(R.id.altEditar);
        deletar     = findViewById(R.id.altDeletar);

        //carregar os dados passando o código como parâmetro
        cursor      = crud.carregaDadosById(Integer.parseInt(codigo));

        /*----------------variavel recebendo os valores---------------*/
        instituicao.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.INSTITUICAO)));
        objetivo.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.OBJETIVO)));
        ligacao.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.TELEFONE)));
        estado.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.ESTADO)));
        cidade.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.CIDADE)));
        necessidade.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.NECESSIDADE)));
        login.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.LOGIN)));
        senha.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.SENHA)));

        /*------------------------Metodos--------------------------*/
        deletar.setOnClickListener(onclickDeletar);
        sair.setOnClickListener(onclickSair);
        editar.setOnClickListener(onclickEditar);

    }
    /*------------------------evento botao deletar-----------------------------*/
        View.OnClickListener onclickDeletar = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Alerta do botao deletar
                AlertDialog alerta;
                AlertDialog.Builder builder = new AlertDialog.Builder(Editar.this);
                builder.setTitle("Deletar dados");
                builder.setMessage("Tem certeza que deseja deletar os seus dados?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       String resultado = crud.deletaRegistro(Integer.parseInt(codigo));
                       Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
                       Intent intent = new Intent(Editar.this,Login.class);
                       startActivity(intent);
                       finish();
                    }
                });
                builder.setNegativeButton("NÃO",null);
                alerta = builder.create();
                alerta.show();
            }
        };
    /*--------------------evento botao sair---------------------------*/
        View.OnClickListener onclickSair = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSair = new Intent(Editar.this,ListaInstituicao.class);
                startActivity(intentSair);
                finish();
            }
        };
    /*--------------------evento botao editar-------------------------*/
        View.OnClickListener onclickEditar = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dadosInstituicao.put("_id", codigo);
                dadosInstituicao.put("instituicao", instituicao.getText().toString());
                dadosInstituicao.put("objetivo", objetivo.getText().toString());
                dadosInstituicao.put("ligacao", ligacao.getText().toString());
                dadosInstituicao.put("estado", estado.getText().toString());
                dadosInstituicao.put("cidade", cidade.getText().toString());
                dadosInstituicao.put("necessidade", necessidade.getText().toString());
                dadosInstituicao.put("login", login.getText().toString());
                dadosInstituicao.put("senha", senha.getText().toString());

                //Alerta do botao editar
                AlertDialog alerta;
                AlertDialog.Builder builder = new AlertDialog.Builder(Editar.this);
                builder.setTitle("Alterar dados");
                builder.setMessage("Tem certeza que deseja alterar os seus dados?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //condicao checando os campos para saber se nao estar vazio
                        if(!checarCampos()) {
                            String resultado = crud.alteraRegistro(dadosInstituicao);
                            Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Editar.this,ListaInstituicao.class);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(getApplicationContext(), "Os campos não podem estar em branco",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("NÃO",null);
                alerta = builder.create();
                alerta.show();
            }
        };

        /*----------------Metodo de checar campos se vazio--------------*/
        public boolean checarCampos(){
            if (instituicao.getText().toString().equals("")
                    || objetivo.getText().toString().equals("")
                    || ligacao.getText().toString().equals("")
                    || estado.getText().toString().equals("")
                    || cidade.getText().toString().equals("")
                    || necessidade.getText().toString().equals("")
                    || login.getText().toString().equals("")
                    || senha.getText().toString().equals("")){
                return true;
            }else{
                return false;
            }
        }
}