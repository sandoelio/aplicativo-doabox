package com.example.sandoelio.doabox.view;


import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandoelio.doabox.banco.BancoController;

import com.example.sandoelio.doabox.R;

public class Login extends AppCompatActivity {

    private EditText login_nome,login_senha;
    private TextView login_cadastrar;
    private Button login_doador,login_entrar;
    private BancoController crud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*------------------Fonte do texto------------------------*/

        String fontPath = "Ban.ttf";
        TextView textView7 = (TextView) findViewById(R.id.textView7);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView7.setTypeface(tf);

        String fontPath2 = "Ban.ttf";
        TextView textView8 = (TextView) findViewById(R.id.textView8);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
        textView8.setTypeface(tf2);

        /*----capturando os valores dos campos pelo ID------*/
        login_nome = findViewById(R.id.login_nome);
        login_senha = findViewById(R.id.login_senha);
        login_cadastrar = findViewById(R.id.login_cadastrar);
        login_doador = findViewById(R.id.login_doador);
        login_entrar = findViewById(R.id.login_entrar);

        //instanciar a classe que faz o controle do banco
        crud = new BancoController(getBaseContext());

        /*----------------Metodos dos botoes--------------------*/
        login_entrar.setOnClickListener(onclickEntrar);
        login_cadastrar.setOnClickListener(onclickCadastrar);
        login_doador.setOnClickListener(onclickDoador);
    }

    /*----------------evento do botao entrar----------------------*/
    View.OnClickListener onclickEntrar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String nome = login_nome.getText().toString();
            String senha = login_senha.getText().toString();
            Integer id = crud.validarAcesso(nome, senha);
            if(id >= 0){
                Intent intent = new Intent(Login.this,ListaInstituicaoEditar.class);
                intent.putExtra("id", id.toString());
                startActivity(intent);
                finish();
            } else{
                Toast.makeText(Login.this, "Login o senha incorreto", Toast.LENGTH_SHORT).show();
            }
        }
    };

    /*---------------evento do botao cadastrar----------------------*/
    View.OnClickListener onclickCadastrar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Login.this,CadastraInstituicao.class);
            startActivity(intent);
        }
    };

    /*-------------------evento do botao doador----------------------*/
    View.OnClickListener onclickDoador = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Login.this,ListaInstituicao.class);
            startActivity(intent);
            finish();
        }
    };

}
