package com.example.sandoelio.doabox.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sandoelio.doabox.R;

public class Apresentacao extends AppCompatActivity {

    private static final int tempoDaBarrinha = 6000;
    private boolean mbactivit;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresentacao);

        /*------------------------Fonte------------------------- */
        String fontPath = "Ban.ttf";
        TextView textView4 = (TextView) findViewById(R.id.textView4);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView4.setTypeface(tf);

        String fontPath2 = "Ban.ttf";
        TextView textView5 = (TextView) findViewById(R.id.textView5);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);
        textView5.setTypeface(tf2);
        /*------------------------------------------------------ */

        mProgressBar = (ProgressBar) findViewById(R.id.barrinha);

        final Thread tempo = new Thread() {
            @Override
            public void run() {
                mbactivit = true;
                try {
                    int waited = 0;
                    while (mbactivit && (waited < tempoDaBarrinha)) {
                        sleep(10);
                        if (mbactivit) {
                            waited += 12;
                            atualizacao(waited);
                        }
                    }
                } catch (InterruptedException e) {

                } finally {
                    Apresentacao.this.start();
                }
            }
        };
        tempo.start();
    }

    public void atualizacao(final int tempoPassado) {
        if (null != mProgressBar) {
            final int progress = mProgressBar.getMax() * tempoPassado / tempoDaBarrinha;
            mProgressBar.setProgress(progress);
        }
    }

    private void start() {
        Intent intent = new Intent(Apresentacao.this,
                Login.class);
        startActivity(intent);
        finish();
    }

}
