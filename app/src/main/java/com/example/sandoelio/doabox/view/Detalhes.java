package com.example.sandoelio.doabox.view;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.sandoelio.doabox.R;
import com.example.sandoelio.doabox.banco.BancoController;
import com.example.sandoelio.doabox.banco.CriaBanco;

public class Detalhes extends AppCompatActivity {
    private TextView instituicao,objetivo,ligacao,estado,cidade,necessidade,login,senha;
    private Button detLig,voltar;
    private Cursor cursor;
    private BancoController crud;
    private String codigo;
    private int number;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        /*------------------------Fonte------------------------- */
        String fontPath = "Ban.ttf";
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView2.setTypeface(tf);

        //recuperado o código passado pela Intent
        codigo = this.getIntent().getStringExtra("codigo");

        //instanciar a classe que faz o controle do banco
        crud = new BancoController(getBaseContext());

        /*----capturando os valores dos campos pelo ID------*/
        instituicao = findViewById(R.id.detInstituicao);
        objetivo    = findViewById(R.id.detObjetivo);
        ligacao     = findViewById(R.id.altLigacao);
        estado      = findViewById(R.id.detEstado);
        cidade      = findViewById(R.id.detCidade);
        necessidade = findViewById(R.id.detNecessidade);
        login       = findViewById(R.id.detLogin);
        senha       = findViewById(R.id.detSenha);
        voltar      = findViewById(R.id.btnVoltar);
        detLig      = findViewById(R.id.detLig);
        cursor      = crud.carregaDadosById(Integer.parseInt(codigo));

        /*---------------------variavel recebendo os valores--------------------------*/
        number = Integer.parseInt((cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.TELEFONE))));
        instituicao.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.INSTITUICAO)));
        objetivo.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.OBJETIVO)));
        ligacao.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.TELEFONE)));
        estado.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.ESTADO)));
        cidade.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.CIDADE)));
        necessidade.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.NECESSIDADE)));
        login.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.LOGIN)));
        senha.setText(cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.SENHA)));

        /*------------------------Metodos dos botoes------------------------------*/
        detLig.setOnClickListener(onclickLig);
        voltar.setOnClickListener(onclickVoltar);

    }
    /*------------------------evento botao ligação-------------------------------*/
        View.OnClickListener onclickLig = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLigacaoContact(number);
            }
        };
    /*------------------------evento botao voltar----------------------------------*/
        View.OnClickListener onclickVoltar = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Detalhes.this,ListaInstituicao.class);
                startActivity(intent);
                finish();
            }
        };
    /*-------------metodo invoca a ligacao para o contato---------------*/
        public void openLigacaoContact(int number) {
            Uri uri = Uri.parse("tel:"+ number);
            Intent intent = new Intent(Intent.ACTION_DIAL,uri);
            startActivity(intent);

        }


}
