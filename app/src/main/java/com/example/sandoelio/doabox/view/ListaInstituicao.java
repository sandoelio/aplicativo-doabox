package com.example.sandoelio.doabox.view;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sandoelio.doabox.AdapterListView;
import com.example.sandoelio.doabox.ItemListaView;
import com.example.sandoelio.doabox.R;
import com.example.sandoelio.doabox.banco.BancoController;
import com.example.sandoelio.doabox.banco.CriaBanco;

import java.util.ArrayList;

public class ListaInstituicao extends Activity {

    private ListView listView;
    private ArrayList<ItemListaView> itens;
    private Button sair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_instituicao);

        // Fonte do texto
        String fontPath = "Ban.ttf";
        TextView textView3 = (TextView) findViewById(R.id.textView3);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView3.setTypeface(tf);

        //Pega o id do ListView e botao
        listView = findViewById(R.id.listView);
        sair     = findViewById(R.id.btnSair);

        //Criamos nossa lista que preenchera o ListView
        itens = new ArrayList<ItemListaView>();

        //instanciar a classe que faz o controle do banco
        BancoController crud = new BancoController(getBaseContext());

        //Cursor com todos os dados recuperados do banco
        final Cursor cursor = crud.carregaDados();
        //mover para a primeira posição para que todos os dados sejam exibidos
        cursor.moveToFirst();

        /*----------retorna verdadeiro quando o cursor está na última posição da linha--------*/
        while (cursor.isAfterLast() == false) {
            // adicionando itens na lista
            itens.add(new ItemListaView(
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.INSTITUICAO)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.OBJETIVO)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.TELEFONE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.ESTADO)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.CIDADE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.NECESSIDADE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.LOGIN)),
                    cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.SENHA)),
                    R.drawable.eco));
            cursor.moveToNext();
        }

        /*---------------------------Adapter-------------------------*/
        listView.setAdapter(new AdapterListView(this, itens));

        /*-------------------Metodo sair do botao-----------------*/
        sair.setOnClickListener(onClickSair);

        /*-------Adicionando o evento click em cada linha do listView---------*/
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String codigo;
                cursor.moveToPosition(position);
                codigo = cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.ID));
                Intent intent = new Intent(ListaInstituicao.this, Detalhes.class);
                intent.putExtra("codigo", codigo);
                startActivity(intent);
                finish();
            }
        });
    }
    /*--------------------evento para sair do botão----------------------------------*/
    View.OnClickListener onClickSair = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ListaInstituicao.this,Login.class);
            startActivity(intent);
            finish();
        }
    };
}

