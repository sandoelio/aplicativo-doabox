package com.example.sandoelio.doabox.view;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.sandoelio.doabox.R;
import com.example.sandoelio.doabox.banco.BancoController;
import java.util.HashMap;
import java.util.Map;

public class CadastraInstituicao extends AppCompatActivity {

    private EditText txtInstituicao,txtObjetivo,txtLigacao,txtEstado,txtCidade,
                     txtNecessidade,txtLogin,txtSenha;
    private Button btnSalvar,btnLimpar;

    // realizar uma busca rápida pela chave que desejamos, sem precisar percorrer toda lista
    private Map<String, String> dados = new HashMap<String, String>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastra_instituicao);

        //Metodo para fonte (letras)
        String fontPath = "Ban.ttf";
        TextView textView = (TextView) findViewById(R.id.textView);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView.setTypeface(tf);

        //capturando os valores dos campos pelo ID
        txtInstituicao = findViewById(R.id.txtInstituicao);
        txtObjetivo = findViewById(R.id.txtObjetivo);
        txtLigacao = findViewById(R.id.txtLigacao);
        txtEstado = findViewById(R.id.txtEstado);
        txtCidade = findViewById(R.id.txtCidade);
        txtNecessidade = findViewById(R.id.txtNecessidade);
        txtLogin = findViewById(R.id.txtLogin);
        txtSenha = findViewById(R.id.txtSenha);
        btnSalvar = findViewById(R.id.btnSalvar);
        btnLimpar = findViewById(R.id.btnLimpar);

        /*-----------------Metodos dos botoes---------------------*/
        btnSalvar.setOnClickListener(onClickSalvar);
        btnLimpar.setOnClickListener(onClickLimpar);

    }

    /*----------evento do click para o botao salvar----------*/
     View.OnClickListener onClickSalvar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //instanciar a classe que faz o controle do banco
            BancoController crud = new BancoController(getBaseContext());

            //passando  .put(chave,valor)
            dados.put("instituicao", txtInstituicao.getText().toString());
            dados.put("objetivo", txtObjetivo.getText().toString());
            dados.put("ligacao", txtLigacao.getText().toString());
            dados.put("estado", txtEstado.getText().toString());
            dados.put("cidade", txtCidade.getText().toString());
            dados.put("necessidade", txtNecessidade.getText().toString());
            dados.put("login", txtLogin.getText().toString());
            dados.put("senha", txtSenha.getText().toString());

            //condicao checando os campos para saber se nao estar vazio
            if (!checarCampos()) {
                String resultado = crud.inserirDados(dados);
                Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getBaseContext(), ListaInstituicao.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Os campos não podem estar em branco",
                        Toast.LENGTH_LONG).show();
            }
        }
    };


    /*----------evento do click para o botao limpar----------------*/
    View.OnClickListener onClickLimpar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            txtInstituicao.setText("");
            txtObjetivo.setText("");
            txtLigacao.setText("");
            txtEstado.setText("");
            txtCidade.setText("");
            txtNecessidade.setText("");
            txtLogin.setText("");
            txtSenha.setText("");
        }
    };


    /*-----------------Metodo de checar campos se vazio-----------------------------*/
    public boolean checarCampos() {
        if (txtInstituicao.getText().toString().equals("") || txtObjetivo.getText().toString().equals("")
                || txtLigacao.getText().toString().equals("") || txtEstado.getText().toString().equals("")
                || txtCidade.getText().toString().equals("") || txtNecessidade.getText().toString().equals("")
                ||txtLogin.getText().toString().equals("")  || txtSenha.getText().toString().equals("")){
            return true;
        } else {
            return false;
        }
    }
}