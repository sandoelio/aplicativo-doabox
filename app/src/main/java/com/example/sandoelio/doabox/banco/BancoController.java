package com.example.sandoelio.doabox.banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Map;

public class BancoController {

    private SQLiteDatabase db;
    private CriaBanco banco;

    /*------construtor enviando o  parametro para activity-----*/
    public BancoController(Context context) {
        banco = new CriaBanco(context);
    }

    /*------mapeia chaves para valores e informações a serem adicionadas no banco------*/
    public String inserirDados(Map<String, String> dados) {
        ContentValues valores;
        long resultado;

        //atributo db recebendo o resultado de getWritableDatabase (escrita e leitura no banco)
        db = banco.getWritableDatabase();
        valores = new ContentValues();

        valores.put(CriaBanco.INSTITUICAO, dados.get(CriaBanco.INSTITUICAO));
        valores.put(CriaBanco.OBJETIVO, dados.get(CriaBanco.OBJETIVO));
        valores.put(CriaBanco.TELEFONE, dados.get(CriaBanco.TELEFONE));
        valores.put(CriaBanco.ESTADO, dados.get(CriaBanco.ESTADO));
        valores.put(CriaBanco.CIDADE, dados.get(CriaBanco.CIDADE));
        valores.put(CriaBanco.NECESSIDADE, dados.get(CriaBanco.NECESSIDADE));
        valores.put(CriaBanco.LOGIN, dados.get(CriaBanco.LOGIN));
        valores.put(CriaBanco.SENHA, dados.get(CriaBanco.SENHA));

        //inserindo na tabela
        resultado = db.insert(CriaBanco.TABELA, null, valores);
        db.close();
        if (resultado == -1) return "Erro ao inserir registro";
        else return "Inserido com sucesso";
    }

    /*----------------metodo que carrega -------------------*/
    public Cursor carregaDados() {
        //salva as informações que são retornadas do banco de dados
        Cursor cursor;
        String[] campos = {CriaBanco.ID, CriaBanco.INSTITUICAO, CriaBanco.OBJETIVO, CriaBanco.TELEFONE,
                           CriaBanco.ESTADO, CriaBanco.CIDADE, CriaBanco.NECESSIDADE, CriaBanco.LOGIN,
                           CriaBanco.SENHA};

        //atributo db recebendo o resultado de getReadableDatabase(somente para leitura)
        db = banco.getReadableDatabase();
        cursor = db.query(CriaBanco.TABELA, campos, null, null, null, null, null, null);
        if (cursor != null) {
            //mover para a primeira posição para que todos os dados sejam exibidos
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    /*------------------Pesquisa pelo id-------------------*/
    public Cursor carregaDadosById(int id) {
        Cursor cursor;
        String[] campos = {CriaBanco.ID, CriaBanco.INSTITUICAO, CriaBanco.OBJETIVO, CriaBanco.TELEFONE, CriaBanco.ESTADO, CriaBanco.CIDADE, CriaBanco.NECESSIDADE, CriaBanco.LOGIN, CriaBanco.SENHA};
        String where = CriaBanco.ID + "=" + id;
        //atributo db recebendo o resultado de getReadableDatabase(somente para leitura)
        db = banco.getReadableDatabase();
        cursor = db.query(CriaBanco.TABELA, campos, where, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    /*--------Altera o registro do cliente pelo id----------*/
    public String alteraRegistro(Map<String, String> dados) {
        ContentValues valores;
        long resultado;
        String where = CriaBanco.ID + "=" + dados.get(CriaBanco.ID);
        //atributo db recebendo o resultado de getWritableDatabase(leitura e escrita)
        db = banco.getWritableDatabase();
        valores = new ContentValues();

        valores.put(CriaBanco.INSTITUICAO, dados.get(CriaBanco.INSTITUICAO));
        valores.put(CriaBanco.OBJETIVO, dados.get(CriaBanco.OBJETIVO));
        valores.put(CriaBanco.TELEFONE, dados.get(CriaBanco.TELEFONE));
        valores.put(CriaBanco.ESTADO, dados.get(CriaBanco.ESTADO));
        valores.put(CriaBanco.CIDADE, dados.get(CriaBanco.CIDADE));
        valores.put(CriaBanco.NECESSIDADE, dados.get(CriaBanco.NECESSIDADE));
        valores.put(CriaBanco.LOGIN, dados.get(CriaBanco.LOGIN));
        valores.put(CriaBanco.SENHA, dados.get(CriaBanco.SENHA));

        resultado = db.update(CriaBanco.TABELA, valores, where, null);
        db.close();

        if (resultado == -1)
            return "Erro ao alterar registro";
        else
            return "Registro alterado com sucesso";
    }

    /*-------------------Exclui o registro------------------*/
    public String deletaRegistro(int id) {
        long resultado;
        String where = CriaBanco.ID + "=" + id;
        //atributo db recebendo o resultado de getReadableDatabase(somente leitura)
        db = banco.getReadableDatabase();
        resultado = db.delete(CriaBanco.TABELA, where, null);
        db.close();

        if (resultado == -1)
            return "Erro ao apagar registro";
        else
            return "Registro deletado com sucesso";
    }

    /*----------metodo de validação do login-----------*/
    public int validarAcesso(String nome, String senha) {
        Cursor cursor;
        String[] campos = {CriaBanco.ID};
        String where = CriaBanco.SENHA + "='" + senha + "' AND " + CriaBanco.LOGIN + "='" + nome + "';";
        //atributo db recebendo o resultado de getReadableDatabase(somente leitura)
        db = banco.getReadableDatabase();
        cursor = db.query(CriaBanco.TABELA, campos, where, null, null, null, null, null);
        Integer result;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CriaBanco.ID)));
        } else {
            result = -1;
        }

        db.close();
        return result;
    }
}

